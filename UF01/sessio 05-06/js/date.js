/***************************
* Exercicis amb numbers    *
****************************/

/******************************************************************
1. Calcula els milisegons que falten fins al dia del teu aniversari.
******************************************************************/
/*

var data_aniversari = new Date(2020, 12, 26, 0, 0, 0, 0);
var data_actual = Date.now();
var dif = data_aniversari.getTime() - data_actual;

alert("Falten " + dif + " mil·lisegons fins el meu aniversari");
*/


/******************************************************************
2. Fes una pàgina amb un botó i que calculi el temps que passa des de que es carrega el document de la página web fins que l’usuari pulsi el botó.
******************************************************************/
/*  Aquest exercic el deixem per més endevat. */


/******************************************************************
3. Demana una Data hora per teclat. 
Què passa si s’assigna amb el mètode setHours el valor 26? 
Què passa si s’assigna amb el mètode setMinutes el valor 65? 
Què passa si s’assigna com a dia de mes 35?
******************************************************************/

/*var today = new Date();
var fecha = prompt("Please enter date.", today.getDate());

if (fecha != null) {
	alert("Hello! You have entered date as: " + fecha);
}

//Si fiquem dades incorrecte, incrementa automaticament
var d = new Date();
alert("data original " + d);
d.setHours(26);
alert("OJO amb les hores  " + d);
d.setMinutes(65);
alert("OJO amb els minuts  " + d);
d.setDate(35);
alert("OJO amb el dia  " + d);
*/


/******************************************************************
4. Demana la data de naixement a un usuari, 
i mostra l’edat en anys, dies, hores, segons i milisegons.
******************************************************************/

/*var input = window.prompt("Introdueix la data de naixement, OJO format YYYY-MM-DD: ");
var fechaNace = new Date(input);

var fechaActual = new Date()
var mes = fechaActual.getMonth();
var dia = fechaActual.getDate();
var año = fechaActual.getFullYear();

fechaActual.setDate(dia);
fechaActual.setMonth(mes);
fechaActual.setFullYear(año);


edad = Math.floor(fechaActual - fechaNace) ;
alert("milisegons " + edad);

edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 )));
alert("segons " + edad);

edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 )));
alert("hores " + edad);

edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24)));
alert("DIES " + edad);

edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365));
alert("ANYS " + edad); */


/******************************************************************
5. Crea un programa que mostri per pantalla la data en format català. 
Per exemple: 11 de setembre de 1714.
******************************************************************/

/*
var fechaActual = new Date()

var dia = fechaActual.getDate();
var diaSemana = fechaActual.getDay();
var mes = fechaActual.getMonth();
var any = fechaActual.getFullYear();

alert( any  )

switch (mes) {
  case 0:
    txtMes = "Gener"; 
	break;
  case 1:
	txtMes = "Febrer"; 
	break;
  case 2:
	txtMes = "Març";
	break;
  case 3:
	txtMes = "Abril";
	break;
  case 4:
	txtMes = "Maig";
	break;	
  case 5:
	txtMes = "Juny";
	break;	
  case 6:
	txtMes = "Juliol";
	break;	
  case 7:
	txtMes = "Agost";
	break;
  case 8:
	txtMes = "Septembre";
	break;	
  case 9:
	txtMes = "Octubre";
	break;
  case 10:
	txtMes = "Novembre";
	break;	
  case 11:
	txtMes = "Desembre";
	break;	
  default:
    alert( "La data té un format incorrecte")    
    break;
}

// fer un altre swith pel dia de la semana

alert(dia + " de " + txtMes + " de " + any );

*/


/******************************************************************
6. Crea un programa que mostri per pantalla l’hora en format català. 
Per exemple: 01:45 tres quarts de dues // Calculem únicament els quarts
******************************************************************/

/*
var now = new Date()
hour = now.getHours();
minutes = now.getMinutes();
output_string_hour_cat = '';
oclock = false;
// MINUTES
if (minutes >= 8 && minutes <= 22) {
		output_string_hour_cat = 'un quart de ';
		hour++;
else if (minutes >= 23 && minutes <= 37) {
		output_string_hour_cat = 'dos quarts de ';
		hour++;
} else if (minutes >= 38 && minutes <= 52) {
		output_string_hour_cat = 'tres quarts de ';
		hour++;
} else {
		oclock = !oclock;
};
			
if (hour > 23) {
	hour -= 24;
}

// HOURS
if (hour == 0 || hour == 12) {
	output_string_hour_cat += 'dotze';
} else if (hour == 1 || hour == 13) {
		output_string_hour_cat += 'una';
} else if(hour == 2 || hour == 14) {
		output_string_hour_cat += 'dos';
} else if(hour == 3 || hour == 15) {
		output_string_hour_cat += 'tres';
} else if(hour == 4 || hour == 16) {
		output_string_hour_cat += 'cuatre';
} else if(hour == 5 || hour == 17) {
		output_string_hour_cat += 'cinc';
} else if(hour == 6 || hour == 18) {
		output_string_hour_cat += 'sis';
} else if(hour == 7 || hour == 19) {
		output_string_hour_cat += 'set';
} else if(hour == 8 || hour == 20) {
		output_string_hour_cat += 'vuit';
} else if(hour == 9 || hour == 21) {
		output_string_hour_cat += 'nou';
} else if(hour == 10 || hour == 22) {
		output_string_hour_cat += 'deu';
} else if(hour == 11 || hour == 23) {
		output_string_hour_cat += 'onze';
};
// Oclock case
if (oclock) {
	output_string_hour_cat += ' en punt';
};
alert(output_string_hour_cat);

*/


/******************************************************************
7. Crea un programa que mostri l’hora per pantalla en format digital i l’actualitzi cada segon.
******************************************************************/

/*
const now = new Date();

let hour = '' + now.getHours()
let minute = '' + now.getMinutes();
let second = '' + now.getSeconds();

if (hour < 10) {
	hour = '0' + hour;
};

if (minute < 10) {
	minute= '0' + minute;
};

if (second < 10) {
    second = '0' + second;
};
				
alert( hour + ":" + minute + ":" + second );
*/

/******************************************************************
8. Crea un programa que mostri l’hora per pantalla en format digital i 
l’actualitzi cada segon. 
******************************************************************/

/* var currentDate = new Date();
    while (true) {
        document.write(currentDate.getHours() + ":" + currentDate.getMinutes() + ":" 
        + currentDate.getSeconds());
        setTimeout(function(){ currentDate.setSeconds(currentDate.getSeconds() + 1); }, 1000);
    }
*/


/******************************************************************
9. Crea un comptador on es mostrin els anys, mesos, dies, hores i segons que falten fins a la inauguració dels pròxims Jocs Olímpics.
******************************************************************/



/******************************************************************
10. Crea un programa on es validi el format de la data introduïda a un formulari.
******************************************************************/


/******************************************************************
11. Crea un programa on es demanin dues dates i digui quina és la més gran.
******************************************************************/

/* var todayDate = new Date(); //Today Date    
var dateOne = new Date(2020, 11, 25);    
if (todayDate > dateOne) {    
    alert("Today Date is greater than Date One.");    
}else {    
    alert("Date One is greater than today .");    
}  */  
   
/******************************************************************
12. Validar si era major edat, en la data donada
******************************************************************/

var day = 12;
var month = 12;
var year = 1996;
var age = 18;
var mydate = new Date();
mydate.setFullYear(year, month-1, day);

var currdate = new Date();
var setDate = new Date();        
setDate.setFullYear(mydate.getFullYear() + age,month-1, day);

alert(currdate);
alert(setDate);

if ((currdate - setDate) > 0){
// you are above 18
	alert("above 18");
}else{
    alert("below 18");
}

















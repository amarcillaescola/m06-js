function getChildNodes(element) {
    let result=[];
    for (var i = 0; i < element.childNodes.length; i++) { // cuenta los hijos primarios que tiene
        if (element.childNodes[i].nodeType==3) { // si es un texto no lo guardamos
            continue;
        }
 
        result.push(element.childNodes[i]);
        if (element.childNodes[i].hasChildNodes()) { // si tiene hijos
            result=result.concat(getChildNodes(element.childNodes[i])); // llamamos la funcion nuevamente
        }
    }
    return result;
}

var element = document.getElementsByTagName("BODY")[0];
const result = getChildNodes(element);
 
console.log(result); // [p, span, a, p]

var element = document.getElementsByTagName("HEAD")[0];
const result2 = getChildNodes(element);
 
console.log(result2); // [p, span, a, p]
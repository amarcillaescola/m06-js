<scrip>
// Guardem l'element en una variable
var primerElement = document.getElementById('primer');

// Creem les funcion que volem que sigui cridada
function mostrarMissatge1() {
  alert("primera funció");
}

function mostrarMissatge2() {
  alert("segona funció");
}

// Subscrivim l'element per que respongui amb les dues funcions
primerElement.addEventListener('click', mostrarMissatge1);

primerElement.addEventListener('click', mostrarMissatge2);

</script>

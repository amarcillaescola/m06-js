///////////////////////////////////////
// Inheritance Between with prototypes.

const Person = function (firstName, birthYear) {
    this.firstName = firstName;
    this.birthYear = birthYear;
  };
  
  Person.prototype.calcAge = function () {
    console.log(2037 - this.birthYear);
  };
  
  const Student = function (firstName, birthYear, course) {
    Person.call(this, firstName, birthYear);
    this.course = course;
  };
  
  console.log("**********      creant persones   ***********"); 
  // Linking prototypes
  Student.prototype = Object.create(Person.prototype);
  
  Student.prototype.introduce = function () {
    console.log(`My name is ${this.firstName} and I study ${this.course}`);
  };
  
  //Creem un nou estudiant 
  const mike = new Student('Mike', 2020, 'Computer Science');

  //introduce és un prototip d'estudiant
  mike.introduce();

  //calcAge és un prototip de persona
  mike.calcAge();


  
  console.log(mike.__proto__);
  console.log(mike.__proto__.__proto__);
  
  console.log(mike instanceof Student);
  console.log(mike instanceof Person);
  console.log(mike instanceof Object);
  
//canviem el constructor
  console.dir(Student.prototype.constructor);
  Student.prototype.constructor = Student;
  console.dir(Student.prototype.constructor);
  
///////////////////////////////////////
// ES6 Classes

// Class expression
// les classes son un especie de funcions
// const PersonCl = class {}
/////////////////////////////////////
//Antiguamente JavaScript no tenía una forma de crear clases, para ello se usaban los prototypes.
// A partir de la ES6, se pueden usar Classes, pero el prototipaje es su manera homonina de siempre.

// Class declaration
class PersonCl {
   // el constructor es un mètode de la classe
    constructor(fullName, birthYear) {
      this.fullName = fullName;
      this.birthYear = birthYear;
    }
  
    // Instance methods
    // Methods will be added to .prototype property

    //AQUEST Mètodes seran dels prototips dels objectes que crearem
    calcAge() {
      console.log(2037 - this.birthYear);
    }
  
    greet() {
      console.log(`Hey ${this.fullName}`);
    }
  
    get age() {
      return 2037 - this.birthYear;
    }
  
    // Set a property that already exists
    set fullName(name) {
      if (name.includes(' ')) this._fullName = name;
      else alert(`${name} is not a full name!`);
    }
  
    get fullName() {
      return this._fullName;
    }
  
    // Static method
    static hey() {
      console.log('Hey there 👋');
      console.log(this);
    }
  }
  
  console.log(""); 
  console.log("**********    208. creant classes ES6  ***********"); 
  console.log("**********      creant persones   ***********"); 
  const jessica = new PersonCl('Jessica Davis', 1996);
  console.log(jessica);
  
  console.log("diferents maneres de calcular la edat."); 
  jessica.calcAge();
  console.log(jessica.age);
  console.log(jessica.__proto__ === PersonCl.prototype); //true
  
  // aquí no seria la milor opció de crear un mètode, l'hem de crear dins de la classe
  // PersonCl.prototype.greet = function () {
  //   console.log(`Hey ${this.firstName}`);
  // };
  jessica.greet();
  
  // 1. Classes are NOT hoisted: Hoisting in javascript is behavior in which all 
  // the declarations are automatically moved on top of the current scope,
  // this behavior actually lets you use a variable or a function before 
  // its declared.

  // 2. Classes are first-class citizes.. 
  // This means the language supports passing functions as arguments to other
  // functions, returning them as the values from other functions, 
  // and assigning them to variables or storing them in data structures. 

  // 3. Classes are executed in strict mode
  // The body of a class is executed in strict mode, i.e., code written here
  // is subject to stricter syntax for increased performance, 
  // some otherwise silent errors will be thrown,
  

  //const walter1 = new PersonCl('Walter', 1965);
  const walter = new PersonCl('Walter White', 1965);
  console.log("prpietats"); 
  //console.log(walter1);
  console.log(walter);
  PersonCl.hey();
  
  
  console.log(""); 
  console.log("**********    209. Getters/Setter  ***********"); 

  ///////////////////////////////////////
  // Setters and Getters
  const account = {
    owner: 'Jonas',
    movements: [200, 530, 120, 300],
  
    get latest() {
      return this.movements.slice(-1).pop();
    },
  
    set latest(mov) {
      this.movements.push(mov);
    },
  };
  
  console.log("**********      get    ***********"); 
  console.log(account.latest);
  
  console.log("**********      set    ***********"); 
  account.latest = 50;
  console.log(account.movements);
  
  console.log(jessica);
  
  ///////////////////////////////////////
  // Object.create
  const PersonProto = {
    calcAge() {
      console.log(2037 - this.birthYear);
    },
  
    init(firstName, birthYear) {
      this.firstName = firstName;
      this.birthYear = birthYear;
    },
  };
  

  console.log("**********      creando personas    ***********"); 

  //constructgor
  const steven = Object.create(PersonProto);
  console.log(steven);
  steven.name = 'Steven';
  steven.birthYear = 2002;
  steven.calcAge();
  
  console.log(steven.__proto__ === PersonProto);
  
  const sarah = Object.create(PersonProto);
  sarah.init('Sarah', 1979);
  sarah.calcAge();
  
  
new Vue({
    el: "#div-10",
    data: {
        url: "https://randomuser.me/api/?results=",
        totalRegistros: 500,
        personas: []
    },
    mounted: function() {
        this.cargarPersonas();
    },
    methods: {
        //https://cdn.jsdelivr.net/npm/vue-resource@1.5.0
        cargarPersonas: function() {
            this.$http.get(this.url + this.totalRegistros)
                .then(function(response) {
                    this.personas = response.body.results;
                });
        }
    }
});

new Vue({
    el: "#div-11",
    data: {
        url: "https://randomuser.me/api/?results=",
        totalRegistros: 500,
        personas: []
    },
    mounted: function() {
        this.cargarPersonas();
    },
    methods: {
        cargarPersonas: function() {
            var _this = this;

            //https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js                
            axios.get(this.url + this.totalRegistros)
                .then(function(response) {
                    _this.personas = response.data.results;
                });
        }
    }
});
new Vue({
    el: '#demo',
    data: {
      back: false,
      currentIndex: 0
    },
    methods: {
      next(){
        this.back = false;
        this.currentIndex++;
      },
      prev(){
        this.back = true;
        this.currentIndex--;
      }
    },
  })